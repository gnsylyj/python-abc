import numpy as np

# numpy array大小固定
array1 = np.array([14, 5, 14, 10])
print(type(array1))

# list大小可以变化
list1 = [123.45, 67, "Joed"]
list1.append(True)
print(list1)

# np.array和list做矩阵运算的消耗时间对比
import time

py_list1 = [i for i in range(10000000)]
#print(py_list1)
start_time = time.process_time()
py_list2 = [i+2 for i in py_list1]
end_time = time.process_time()
print(end_time - start_time)

np_array1 = np.arange(10000000)
start_time = time.process_time()
np_array2 =np_array1 + 2
end_time = time.process_time()
print(end_time - start_time)

'''
0.515625
0.015625
50倍
'''

# 创建numpy arrays,随机赋值
empty_array = np.empty(5, np.int32)
print(empty_array)

'''
[1157766464      32763 1157770976      32763          0]
'''

#创建值为0的矩阵
zero_array = np.zeros(5, np.int32)
print(zero_array)

'''
[0 0 0 0 0]
'''

zero_array = np.zeros((3,5), np.int32)
print(zero_array)

'''
[[0 0 0 0 0]
 [0 0 0 0 0]
 [0 0 0 0 0]]
'''

#创建值为1的矩阵
one_array = np.ones((5,4), np.int32)
print(one_array)

'''
[[1 1 1 1]
 [1 1 1 1]
 [1 1 1 1]
 [1 1 1 1]
 [1 1 1 1]]
'''

np_array = np.array([[90,89,78],[77,99,88]])
print(np_array)

'''
[[90 89 78]
 [77 99 88]]
'''

another_array = np.zeros_like(np_array)
print(another_array)

'''
[[0 0 0]
 [0 0 0]]
'''

another_array = np.ones_like(np_array)
print(another_array)

'''
[[1 1 1]
 [1 1 1]]
'''

linear = np.linspace(1, 10, 15)
print(linear)

'''
[ 1.          1.64285714  2.28571429  2.92857143  3.57142857  4.21428571
  4.85714286  5.5         6.14285714  6.78571429  7.42857143  8.07142857
  8.71428571  9.35714286 10.        ]
'''

# pip install matplotlib

import matplotlib.pyplot as plt
print(linear)
plt.plot(linear, "x")
#plt.show()

# 随机产生由1到100中5个值组成的numpy array
r = np.random.randint(1, 100, 5)
print(r)

'''
[93  1  6 67  1]
'''

# 随机产生由1到100组成的numpy array, shapt为3行5列
r = np.random.randint(1, 100, (3,5))
print(r)

'''
[[69 59 57 25 77]
 [63 82 97 47 73]
 [ 6 88 79 80 93]]
'''

#ndim为维度
print(r.ndim)

#shape为行数和列数
print(r.shape)

#size为元素个数
print(r.size)

#dtype为元素类型
print(r.dtype)

#itemsize元素类型的长度
print(r.itemsize)

'''
2
(3, 5)
15
int32
4
'''

# 文件读取

import random
outfile = open("id.txt","w", )
for i in range(1000):
    rdnum = random.randint(1,100)
    outfile.write(f"{rdnum} ")
outfile.close()

txtdata = open("id.txt","r").readlines()
print(txtdata)

print(len(txtdata))

print(type(txtdata))

txtdata = np.loadtxt("id.txt")
print(txtdata)

txtdata = np.loadtxt("id.txt", np.int32)
print(txtdata)

print(txtdata.shape)

print(txtdata.reshape(500,2))

print(txtdata.size)

outfile = open("id.txt","w", )
for i in range(648830):
    rdnum = random.randint(0,255)
    outfile.write(f"{rdnum} ")
outfile.close()
txtdata = np.loadtxt("id.txt", np.int32)
txtdata = txtdata.reshape(805,806)
plt.imshow(txtdata)
plt.imshow(txtdata, cmap="gray")
#plt.show()

plt.imshow(txtdata[200:400, 200:400])
#plt.show()

flipv=txtdata[::-1,:]
#plt.show()

#数学和统计
x = np.linspace(0, 5 * np.pi, 64)
print(x)

'''
[ 0.          0.24933275  0.4986655   0.74799825  0.997331    1.24666375
  1.4959965   1.74532925  1.994662    2.24399475  2.4933275   2.74266025
  2.991993    3.24132575  3.4906585   3.73999125  3.989324    4.23865675
  4.48798951  4.73732226  4.98665501  5.23598776  5.48532051  5.73465326
  5.98398601  6.23331876  6.48265151  6.73198426  6.98131701  7.23064976
  7.47998251  7.72931526  7.97864801  8.22798076  8.47731351  8.72664626
  8.97597901  9.22531176  9.47464451  9.72397726  9.97331001 10.22264276
 10.47197551 10.72130826 10.97064101 11.21997376 11.46930651 11.71863926
 11.96797201 12.21730476 12.46663751 12.71597026 12.96530301 13.21463577
 13.46396852 13.71330127 13.96263402 14.21196677 14.46129952 14.71063227
 14.95996502 15.20929777 15.45863052 15.70796327]
'''
y = np.sin(x)
plt.close()
plt.plot(x,y)
#plt.show()
plt.close()
plt.plot(x, np.cos(x))
plt.plot(x, np.log(1+x))
#plt.show()
plt.close()

scores = np.random.randint(0, 101, 25)
print(scores)
plt.plot(scores, "o")
#plt.show()
print(np.median(scores))

print(np.std(scores))

print(np.min())

print(np.max())