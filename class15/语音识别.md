# 语音识别

https://pypi.org/project/SpeechRecognition/

https://github.com/Uberi/speech_recognition/blob/master/reference/pocketsphinx.rst#installing-other-languages

## 1. 安装环境

```bash
sudo apt-get install python-pyaudio -y
sudo apt-get install python3-pyaudio -y
sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio 
pip install --upgrade pip
pip install pyaudio
pip install SpeechRecognition
pip install pyttsx3
```

## 2. 程序

```python
import speech_recognition
import pyttsx3

recongnizer = speech_recognition.Recognizer()

while True:
    try:
        with speech_recognition.Microphone() as mic:
            recongnizer.adjust_for_ambient_noise(mic, duration=0.2)
            audio = recongnizer.listen(mic)
            text = recongnizer.recognize_google(audio)
            text = text.lower()
            print(f"Recognized {text}")
    except speech_recognition.UnknownValueError():
        recongnizer = speech_recognition.Recognizer()
        continue
```