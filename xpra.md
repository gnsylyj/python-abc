```bash
wget -q https://xpra.org/gpg.asc -O- | sudo apt-key add -
wget https://xpra.org/repos/bionic/xpra.list -O /etc/apt/sources.list.d/xpra.list
wget https://xpra.org/repos/focal/xpra.list -O /etc/apt/sources.list.d/xpra.list
apt update
apt install xpra -y
apt-get clean -y


nohup xpra start --bind-tcp=127.0.0.1:6789 --html=on --speed=1 --encoding=x265 --notifications=no --pixel-depth=16 --compress=9 --auto-refresh-delay=5 --quality=70 --start=terminator &
```